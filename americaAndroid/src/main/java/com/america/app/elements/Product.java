package com.america.app.elements;

public class Product {
	
    // Productos Table Columns names
    private String KEY_EAN;
    private String NOM_LARGO;
    private String NOM_CORTO;
    private int COD_DEPTO;
    private int TIPO_VTA;
    private double P_FINAL;
    private double PS_IVA;
    private double GANANCIA;
    private double PREC_VTA;
    private double PREC_VTA_ANTERIOR;
    private int STOCK;
    private int CANT_NUEVOS;
    private int CANT_ENVASADA;
    private int UNIDAD_ENVASADO;
    private boolean SYNC_STATUS; //variable de sincronizacion
    private boolean PRECIO_MODIFICADO; //variable que me indica si el precio fue modificado
    
    public Product(String id) {
		this.setKEY_EAN(id);
		this.setNOM_LARGO(null);
		this.setNOM_CORTO(null);
		this.setCOD_DEPTO(0);
		this.setTIPO_VTA(0);
		this.setP_FINAL(0);
		this.setPS_IVA(0);
		this.setGANANCIA(0);
		this.setPREC_VTA(0);
		this.setPREC_VTA_ANTERIOR(0);
		this.setSTOCK(0);
		this.setCANT_NUEVOS(0);
		this.setCANT_ENVASADA(0);
		this.setUNIDAD_ENVASADO(0);
		this.setSYNC_STATUS(true);
		this.setPRECIO_MODIFICADO(false);
	}
    
	public String getKEY_EAN() {
		return KEY_EAN;
	}


	public void setKEY_EAN(String kEY_EAN) {
		KEY_EAN = kEY_EAN;
	}


	public String getNOM_LARGO() {
		return NOM_LARGO;
	}


	public void setNOM_LARGO(String nOM_LARGO) {
		NOM_LARGO = nOM_LARGO;
	}


	public String getNOM_CORTO() {
		return NOM_CORTO;
	}


	public void setNOM_CORTO(String nOM_CORTO) {
		NOM_CORTO = nOM_CORTO;
	}


	public int getCOD_DEPTO() {
		return COD_DEPTO;
	}


	public void setCOD_DEPTO(int cOD_DEPTO) {
		COD_DEPTO = cOD_DEPTO;
	}


	public int getTIPO_VTA() {
		return TIPO_VTA;
	}


	public void setTIPO_VTA(int tIPO_VTA) {
		TIPO_VTA = tIPO_VTA;
	}


	public double getP_FINAL() {
		return P_FINAL;
	}


	public void setP_FINAL(double p_FINAL) {
		P_FINAL = p_FINAL;
	}


	public double getPS_IVA() {
		return PS_IVA;
	}


	public void setPS_IVA(double pS_IVA) {
		PS_IVA = pS_IVA;
	}


	public double getGANANCIA() {
		return GANANCIA;
	}


	public void setGANANCIA(double gANANCIA) {
		GANANCIA = gANANCIA;
	}


	public double getPREC_VTA() {
		return PREC_VTA;
	}


	public void setPREC_VTA(double pREC_VTA) {
		PREC_VTA = pREC_VTA;
	}


	public int getSTOCK() {
		return STOCK;
	}


	public void setSTOCK(int sTOCK) {
		STOCK = sTOCK;
	}

	public boolean isSYNC_STATUS() {
		return SYNC_STATUS;
	}


	public void setSYNC_STATUS(boolean sYNC_STATUS) {
		SYNC_STATUS = sYNC_STATUS;
	}


	public int getCANT_ENVASADA() {
		return CANT_ENVASADA;
	}


	public void setCANT_ENVASADA(int cANT_ENVASADA) {
		CANT_ENVASADA = cANT_ENVASADA;
	}


	public int getUNIDAD_ENVASADO() {
		return UNIDAD_ENVASADO;
	}


	public void setUNIDAD_ENVASADO(int uNIDAD_ENVASADO) {
		UNIDAD_ENVASADO = uNIDAD_ENVASADO;
	}


	public double getPREC_VTA_ANTERIOR() {
		return PREC_VTA_ANTERIOR;
	}


	public void setPREC_VTA_ANTERIOR(double pREC_VTA_ANTERIOR) {
		PREC_VTA_ANTERIOR = pREC_VTA_ANTERIOR;
	}


	public int getCANT_NUEVOS() {
		return CANT_NUEVOS;
	}


	public void setCANT_NUEVOS(int cANT_NUEVOS) {
		CANT_NUEVOS = cANT_NUEVOS;
	}

	public boolean isPRECIO_MODIFICADO() {
		return PRECIO_MODIFICADO;
	}

	public void setPRECIO_MODIFICADO(boolean pRECIO_MODIFICADO) {
		PRECIO_MODIFICADO = pRECIO_MODIFICADO;
	}
    
}

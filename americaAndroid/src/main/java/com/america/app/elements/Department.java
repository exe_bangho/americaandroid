package com.america.app.elements;

public class Department {
	
	private int CODE; 
	private String NAME;
	private boolean DELETE;
	private double IVA;
	private double MAXIMO;
	
	public Department(){
		
	}
	
	public int getCODE() {
		return CODE;
	}
	public void setCODE(int cODE) {
		CODE = cODE;
	}
	public String getNAME() {
		return NAME;
	}
	public void setNAME(String nAME) {
		NAME = nAME;
	}
	public boolean isDELETE() {
		return DELETE;
	}
	public void setDELETE(boolean dELETE) {
		DELETE = dELETE;
	}
	public double getIVA() {
		return IVA;
	}
	public void setIVA(double iVA) {
		IVA = iVA;
	}

	public double getMAXIMO() {
		return MAXIMO;
	}

	public void setMAXIMO(double mAXIMO) {
		MAXIMO = mAXIMO;
	}


}

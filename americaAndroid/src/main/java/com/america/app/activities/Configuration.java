package com.america.app.activities;

import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.america.R;
import com.america.app.AmericaApplication;
import com.america.app.utils.MyConstants;

public class Configuration extends BaseActivity{

	private static final String PORT_DEFAULT = "80";
	private EditText editTextPort;
	private EditText editTextIp;
	
	private MyConstants constantes;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		//Remover la barra de t�tulo
		//requestWindowFeature(Window.FEATURE_NO_TITLE);

		//Remover la barra de notificaciones
		// getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

		//Aplicacion sin modo ahorro de energia
		//getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		//vista vertical
		setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);  
		
		setContentView(R.layout.activity_configuration);
		editTextPort = (EditText) findViewById(R.id.editTextPort);
		editTextIp = (EditText) findViewById(R.id.editTextIp);
		
		loadConfiguration();
	}
	
	
	private void loadConfiguration() {
		SharedPreferences prefs = AmericaApplication.getInstance().getPrefs(); 
		editTextPort.setText(prefs.getString(constantes.PORT, PORT_DEFAULT));
		editTextIp.setText(prefs.getString(constantes.IP, ""));
	}


	public void saveConfiguration(View v){
		SharedPreferences prefs = AmericaApplication.getInstance().getPrefs();
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString(constantes.PORT, editTextPort.getText().toString());
		editor.putString(constantes.IP, editTextIp.getText().toString());
		editor.commit();		
    	this.finish();
	}
}

package com.america.app.activities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.america.R;
import com.america.app.AmericaApplication;
import com.america.app.adapters.DepartmentAdapter;
import com.america.app.database.DBDepartment;
import com.america.app.database.DBProduct;
import com.america.app.elements.Department;
import com.america.app.elements.Product;
import com.america.app.utils.DecimalDigitsInputFilter;
import com.america.app.utils.DecimalRound;
import com.america.app.utils.MySpinner;

public class ProductInfo extends BaseActivity{

	private Product producto;
	private EditText EAN, NOM_LARGO, NOM_CORTO, P_FINAL, PS_IVA, GANANCIA, PREC_VTA, PREC_VTA_ANTERIOR, STOCK, IVA, CANT_NUEVOS, CANT_ENVASADA;
	private MySpinner spinnerDepartment;
	private Spinner spinnerUnidadEnvasado, spinnerTipoVenta;
	private DepartmentAdapter departmentAdapter;
	private boolean firstEntry = true;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);		
		
		//Remover la barra de t�tulo
		//requestWindowFeature(Window.FEATURE_NO_TITLE);

		//Remover la barra de notificaciones
		// getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

		//Aplicacion sin modo ahorro de energia
		//getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		//vista vertical
		setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);  
		
		setContentView(R.layout.activity_productinfo);
		
		String idProduct = "";
        //Recibo parametro
        Bundle extras = getIntent().getExtras();
        if(extras !=null){
        	idProduct = extras.getString("idProduct");
        }
        
        AmericaApplication.getInstance().setCurrentActivity(this);

        spinnerDepartment = (MySpinner) findViewById(R.id.spinnerDepartment);
        spinnerDepartment.SetTextSpinnerDefault("Department");
        List<Department> lDepartment = DBDepartment.getDepartments();
        Department[] aDepartment = new Department[lDepartment.size()];
        aDepartment = lDepartment.toArray(aDepartment);
        departmentAdapter = new DepartmentAdapter(AmericaApplication.getInstance().getCurrentActivity(), android.R.layout.simple_spinner_dropdown_item, aDepartment);
        spinnerDepartment.setAdapter(departmentAdapter);
        
        spinnerUnidadEnvasado = (Spinner) findViewById(R.id.spinnerUNIDAD_ENVASADO);
        loadUnidadEnvasado();

        spinnerTipoVenta = (Spinner) findViewById(R.id.spinnerTIPO_VTA);
        loadTipoVenta();
        
        EAN = (EditText) findViewById(R.id.editTextEAN);
        NOM_LARGO = (EditText) findViewById(R.id.editTextNOM_LARGO);
        NOM_CORTO = (EditText) findViewById(R.id.editTextNOM_CORTO);
        P_FINAL = (EditText) findViewById(R.id.editTextP_FINAL);
        PS_IVA = (EditText) findViewById(R.id.editTextPS_IVA);
        IVA = (EditText) findViewById(R.id.editTextIVA);
        GANANCIA = (EditText) findViewById(R.id.editTextGANANCIA);
        PREC_VTA = (EditText) findViewById(R.id.editTextPREC_VTA);
        PREC_VTA_ANTERIOR = (EditText) findViewById(R.id.editTextPREC_VTA_ANTERIOR);
        STOCK = (EditText) findViewById(R.id.editTextSTOCK);
        CANT_NUEVOS = (EditText) findViewById(R.id.editTextCANT_NUEVOS);
        CANT_ENVASADA = (EditText) findViewById(R.id.editTextCANT_ENVASADA);
                
        PREC_VTA.addTextChangedListener(new MyTextWatcher(PREC_VTA));
        P_FINAL.addTextChangedListener(new MyTextWatcher(P_FINAL));
        GANANCIA.addTextChangedListener(new MyTextWatcher(GANANCIA));
        PS_IVA.addTextChangedListener(new MyTextWatcher(PS_IVA));
        
        EAN.setOnFocusChangeListener(new MyOnFocusChangeListener());
        NOM_LARGO.setOnFocusChangeListener(new MyOnFocusChangeListener());
        PREC_VTA.setOnFocusChangeListener(new MyOnFocusChangeListener());
        P_FINAL.setOnFocusChangeListener(new MyOnFocusChangeListener());
        GANANCIA.setOnFocusChangeListener(new MyOnFocusChangeListener());
        PS_IVA.setOnFocusChangeListener(new MyOnFocusChangeListener());

        PREC_VTA_ANTERIOR.setFilters(new InputFilter[] {new DecimalDigitsInputFilter(22,2)});
        PREC_VTA.setFilters(new InputFilter[] {new DecimalDigitsInputFilter(22,2)});
        P_FINAL.setFilters(new InputFilter[] {new DecimalDigitsInputFilter(22,2)});
        GANANCIA.setFilters(new InputFilter[] {new DecimalDigitsInputFilter(22,2)});
        PS_IVA.setFilters(new InputFilter[] {new DecimalDigitsInputFilter(22,2)});

        producto = DBProduct.getProduct(idProduct);
        if (producto == null)
			producto = new Product(idProduct);

		setDataOfProduct();
		
//		PREC_VTA_ANTERIOR.setKeyListener(DigitsKeyListener.getInstance(true,true));
//		PREC_VTA.setKeyListener(DigitsKeyListener.getInstance(true,true));
//		P_FINAL.setKeyListener(DigitsKeyListener.getInstance(true,true));
//		GANANCIA.setKeyListener(DigitsKeyListener.getInstance(true,true));
//		PS_IVA.setKeyListener(DigitsKeyListener.getInstance(true,true));


        spinnerDepartment.setOnItemSelectedListener(new OnItemSelectedListener() {
        	@Override
        	public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        		Department deptoSelected = (Department) (spinnerDepartment.getSelectedItem());
        		IVA.setText(String.valueOf(deptoSelected.getIVA()));
        		if (!firstEntry) {
        			PREC_VTA_ANTERIOR.setText(String.valueOf(DecimalRound.round(producto.getPREC_VTA(), 2)));
        			P_FINAL.setText(String.valueOf(DecimalRound.round(Double.valueOf(PS_IVA.getText().toString()) * (1 + (Double.valueOf(IVA.getText().toString()) / 100)), 2)));
        			PREC_VTA.setText(String.valueOf(DecimalRound.round(Double.valueOf(P_FINAL.getText().toString()) * (1 + (Double.valueOf(GANANCIA.getText().toString()) / 100)), 2)));
        			producto.setPRECIO_MODIFICADO(true);	
				} else {
					firstEntry = false;
				}
        	}
        	@Override
        	public void onNothingSelected(AdapterView<?> parent) {
        	}
        });
	}
	

	private void loadUnidadEnvasado() {
		   ArrayList<String> lista = new ArrayList<String>();
		   //0 a 8
		   lista.add("Ninguna");
		   lista.add("CC");
		   lista.add("CM");
		   lista.add("G");
		   lista.add("KG");
		   lista.add("L");
		   lista.add("M");
		   lista.add("ML");
		   lista.add("U");
		   ArrayAdapter<String> adaptador = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, lista);
		   adaptador.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		   spinnerUnidadEnvasado.setAdapter(adaptador);
		}

	private void loadTipoVenta() {
		ArrayList<String> lista = new ArrayList<String>();
		lista.add("No Balanza");
		lista.add("No Pesable");
		lista.add("Pesable");
		lista.add("Peso Fijo");
		ArrayAdapter<String> adaptador = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, lista);
		adaptador.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnerTipoVenta.setAdapter(adaptador);
	}

	private void setDataOfProduct() {
		int codDepto = departmentAdapter.getPosicion(producto.getCOD_DEPTO());
		if(codDepto != -1)
			spinnerDepartment.setSelection(departmentAdapter.getPosicion(producto.getCOD_DEPTO()));
		else if (!departmentAdapter.isEmpty()) {// Si no tiene seleccionado ningun departamento, se setea el primero de la lista
				spinnerDepartment.setSelection(0);
		}
		
		EAN.setText(producto.getKEY_EAN());
		
		if (producto.getNOM_LARGO() != null)
			NOM_LARGO.setText(producto.getNOM_LARGO());
		else NOM_LARGO.setText("");
		
		if(producto.getNOM_CORTO() != null)
			NOM_CORTO.setText(producto.getNOM_CORTO());
		else NOM_CORTO.setText("");
	
		P_FINAL.setText(String.valueOf(DecimalRound.round(producto.getP_FINAL(), 2)));
		PS_IVA.setText(String.valueOf(DecimalRound.round(producto.getPS_IVA(), 2)));
		IVA.setText(String.valueOf(0));
		GANANCIA.setText(String.valueOf(DecimalRound.round(producto.getGANANCIA(), 2)));
		PREC_VTA.setText(String.valueOf(DecimalRound.round(producto.getPREC_VTA(), 2)));
		PREC_VTA_ANTERIOR.setText(String.valueOf(DecimalRound.round(producto.getPREC_VTA_ANTERIOR(), 2)));
		STOCK.setText(String.valueOf(producto.getSTOCK()));
		CANT_NUEVOS.setText(String.valueOf(producto.getCANT_NUEVOS()));
		CANT_ENVASADA.setText(String.valueOf(producto.getCANT_ENVASADA()));
		spinnerTipoVenta.setSelection(producto.getTIPO_VTA());
		spinnerUnidadEnvasado.setSelection(producto.getUNIDAD_ENVASADO());
	}

	private void getDataOfProduct() {
		Department deptoSelected = ((Department) spinnerDepartment.getSelectedItem());
		if (deptoSelected != null) {
			producto.setCOD_DEPTO(deptoSelected.getCODE());
		} 
		if(NOM_LARGO.getText().toString().compareToIgnoreCase("") != 0)
			producto.setNOM_LARGO(NOM_LARGO.getText().toString());
		if(NOM_CORTO.getText().toString().compareToIgnoreCase("") != 0)
			producto.setNOM_CORTO(NOM_CORTO.getText().toString());
		if(P_FINAL.getText().toString().compareToIgnoreCase("") != 0)
			producto.setP_FINAL(Double.parseDouble(P_FINAL.getText().toString()));
		if(PS_IVA.getText().toString().compareToIgnoreCase("") != 0)
			producto.setPS_IVA(Double.parseDouble(PS_IVA.getText().toString()));
		if(GANANCIA.getText().toString().compareToIgnoreCase("") != 0)
			producto.setGANANCIA(Double.parseDouble(GANANCIA.getText().toString()));
		if(PREC_VTA.getText().toString().compareToIgnoreCase("") != 0)
			producto.setPREC_VTA(Double.parseDouble(PREC_VTA.getText().toString()));
		if(PREC_VTA_ANTERIOR.getText().toString().compareToIgnoreCase("") != 0)
			producto.setPREC_VTA_ANTERIOR(Double.parseDouble(PREC_VTA_ANTERIOR.getText().toString()));
		if(STOCK.getText().toString().compareToIgnoreCase("") != 0)
			producto.setSTOCK(Integer.valueOf(STOCK.getText().toString()));
		if(CANT_NUEVOS.getText().toString().compareToIgnoreCase("") != 0)
			producto.setCANT_NUEVOS(Integer.valueOf(CANT_NUEVOS.getText().toString()));
		if(CANT_ENVASADA.getText().toString().compareToIgnoreCase("") != 0)
			producto.setCANT_ENVASADA(Integer.valueOf(CANT_ENVASADA.getText().toString()));
		producto.setTIPO_VTA(spinnerTipoVenta.getSelectedItemPosition());
		producto.setUNIDAD_ENVASADO(spinnerUnidadEnvasado.getSelectedItemPosition());
		
	}
	
	public Map<String, String> getValidation(Map<String, String> mapValidation){
		
		Department deptoSelected = ((Department) spinnerDepartment.getSelectedItem());
		if (deptoSelected == null) {
			mapValidation.put("COD_DEPTO", "Tipo INCOMPLETO");
		}
		if(EAN.getText().toString().compareToIgnoreCase("") == 0)
			mapValidation.put("EAN", "String INCOMPLETO");
		if(NOM_LARGO.getText().toString().compareToIgnoreCase("") == 0)
			mapValidation.put("NOM_LARGO", "String INCOMPLETO");
		if(NOM_CORTO.getText().toString().compareToIgnoreCase("") == 0)
			mapValidation.put("NOM_CORTO", "String INCOMPLETO");
		if(STOCK.getText().toString().compareToIgnoreCase("") == 0)
			mapValidation.put("STOCK", "Integer INCOMPLETO");
		if(CANT_NUEVOS.getText().toString().compareToIgnoreCase("") == 0)
			mapValidation.put("CANT_NUEVOS", "Integer INCOMPLETO");
		if(CANT_ENVASADA.getText().toString().compareToIgnoreCase("") == 0)
			mapValidation.put("CANT_ENVASADA", "Integer INCOMPLETO");
		mapValidation = getValidationValuesOfPrice(mapValidation);
		
		return mapValidation;
	}
	
	public Map<String, String> getValidationValuesOfPrice(Map<String, String> mapValidation){
		if((P_FINAL.getText().toString().compareToIgnoreCase("") == 0) || (P_FINAL.getText().toString().compareToIgnoreCase(".") == 0))
			mapValidation.put("P_FINAL", "Double INCOMPLETO");
		if((PS_IVA.getText().toString().compareToIgnoreCase("") == 0) || (PS_IVA.getText().toString().compareToIgnoreCase(".") == 0))
			mapValidation.put("PS_IVA", "Double INCOMPLETO");
		if((IVA.getText().toString().compareToIgnoreCase("") == 0) || (IVA.getText().toString().compareToIgnoreCase(".") == 0))
			mapValidation.put("IVA", "Double INCOMPLETO");
		if((GANANCIA.getText().toString().compareToIgnoreCase("") == 0) || (GANANCIA.getText().toString().compareToIgnoreCase(".") == 0))
			mapValidation.put("GANANCIA", "Double INCOMPLETO");
		if((PREC_VTA.getText().toString().compareToIgnoreCase("") == 0) || (PREC_VTA.getText().toString().compareToIgnoreCase(".") == 0))
			mapValidation.put("PREC_VTA", "Double INCOMPLETO");
		if((PREC_VTA_ANTERIOR.getText().toString().compareToIgnoreCase("") == 0) || (PREC_VTA_ANTERIOR.getText().toString().compareToIgnoreCase(".") == 0))
			mapValidation.put("PREC_VTA_ANTERIOR", "Double INCOMPLETO");
		return mapValidation;
	}

	public void setChangeofNameLarge(String name){
		int cantEnvasada = 0;
		String unidadEnvasada;
		String[] separated = name.split(" ");
		if (separated.length >= 3) {
			try {
				cantEnvasada = Integer.valueOf(separated[separated.length-2]);				
			} catch (Exception e) {
				// TODO: handle exception
			}
			CANT_ENVASADA.setText(String.valueOf(cantEnvasada)); // es int???????????????????
			
			Map<String, Integer> mapUnidadEnvasada = new HashMap<String, Integer>();
			mapUnidadEnvasada.put("Ninguna", 0);
			mapUnidadEnvasada.put("CC", 1);
			mapUnidadEnvasada.put("CM", 2);
			mapUnidadEnvasada.put("G", 3);
			mapUnidadEnvasada.put("KG", 4);
			mapUnidadEnvasada.put("L", 5);
			mapUnidadEnvasada.put("M", 6);
			mapUnidadEnvasada.put("ML", 7);
			mapUnidadEnvasada.put("U", 8);
			
			unidadEnvasada = (separated[separated.length-1]).toUpperCase();
			if (mapUnidadEnvasada.containsKey(unidadEnvasada)) {
				spinnerUnidadEnvasado.setSelection(mapUnidadEnvasada.get(unidadEnvasada));
			}
		}
		
	}
	
	@SuppressWarnings("rawtypes")
	public void productSave(View v) {
		Map<String, String> mapValidation = new HashMap<String, String>();
		mapValidation = getValidation(mapValidation);
		Iterator iterador = mapValidation.keySet().iterator();
		if (iterador.hasNext()) {
			viewErrors(mapValidation);
			Toast toast = Toast.makeText(AmericaApplication.getInstance().getCurrentActivity(), AmericaApplication.getInstance().getCurrentActivity().getString(R.string.Valid_data), Toast.LENGTH_LONG);
			toast.setGravity(Gravity.CENTER, 0, 0);
			toast.show();
		} else {
			getDataOfProduct();
			producto.setSYNC_STATUS(false);
			if (DBProduct.updateProduct(producto) == 0) {
				DBProduct.addProduct(producto);
			}
			finish();
		}
	}

	private void viewErrors(Map<String, String> mapValidation) {
		if (mapValidation.containsKey("NOM_LARGO")) {
			NOM_LARGO.setBackgroundResource(R.drawable.backtext);
		}else {
			NOM_LARGO.setBackgroundResource(android.R.drawable.edit_text);
		}
		if (mapValidation.containsKey("NOM_CORTO")) {
			NOM_CORTO.setBackgroundResource(R.drawable.backtext);
		}else {
			NOM_CORTO.setBackgroundResource(android.R.drawable.edit_text);
		}
//		if (mapValidation.containsKey("COD_DEPTO")) {
//			spinnerDepartment.setBackgroundResource(android.R.color.holo_red_dark);
//		}else {
//			spinnerDepartment.setBackgroundResource(android.R.drawable.spinner_dropdown_background);
//		}
//		if (mapValidation.containsKey("STOCK")) {
//			STOCK.setBackgroundResource(R.drawable.backtext);
//		}else {
//			STOCK.setBackgroundResource(android.R.drawable.edit_text);
//		}
		if (mapValidation.containsKey("CANT_NUEVOS")) {
			CANT_NUEVOS.setBackgroundResource(R.drawable.backtext);
		}else {
			CANT_NUEVOS.setBackgroundResource(android.R.drawable.edit_text);
		}
		if (mapValidation.containsKey("CANT_ENVASADA")) {
			CANT_ENVASADA.setBackgroundResource(R.drawable.backtext);
		}else {
			CANT_ENVASADA.setBackgroundResource(android.R.drawable.edit_text);
		}
		
		viewErrorsValuesOfPrice(mapValidation);
	}
	
	private void viewErrorsValuesOfPrice(Map<String, String> mapValidation) {

		if (mapValidation.containsKey("P_FINAL")) {
			P_FINAL.setBackgroundResource(R.drawable.backtext);
		}else {
			P_FINAL.setBackgroundResource(android.R.drawable.edit_text);
		}
		if (mapValidation.containsKey("PS_IVA")) {
			PS_IVA.setBackgroundResource(R.drawable.backtext);
		}else {
			PS_IVA.setBackgroundResource(android.R.drawable.edit_text);
		}
//		if (mapValidation.containsKey("IVA")) {
//			IVA.setBackgroundResource(R.drawable.backtext);
//		}else {
//			IVA.setBackgroundResource(android.R.drawable.edit_text);
//		}
		if (mapValidation.containsKey("GANANCIA")) {
			GANANCIA.setBackgroundResource(R.drawable.backtext);
		}else {
			GANANCIA.setBackgroundResource(android.R.drawable.edit_text);
		}
		if (mapValidation.containsKey("PREC_VTA")) {
			PREC_VTA.setBackgroundResource(R.drawable.backtext);
		}else {
			PREC_VTA.setBackgroundResource(android.R.drawable.edit_text);
		}
//		if (mapValidation.containsKey("PREC_VTA_ANTERIOR")) {
//			PREC_VTA_ANTERIOR.setBackgroundResource(R.drawable.backtext);
//		}else {
//			PREC_VTA_ANTERIOR.setBackgroundResource(android.R.drawable.edit_text);
//		}
	}
	
	public class MyTextWatcher implements TextWatcher{

		private View view;

	    public MyTextWatcher(View view) {
	        this.view = view; 
	    }
		
		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
			// TODO Auto-generated method stub
			
		}


//		PFinal = PSIVA + PSIVA*IVA ===> PFinal = PSIVA * (1 + IVA) ===> PSIVA = PFinal / (1 + IVA)
		//presio sin iva me modifica precio final y precio de venta
		
//		PVENTA = PFINAL + PFINAL*GANANCIA ===> PVENTA = PFINAL * (1 + GANANCIA) ===> PFINAL = PVENTA / (1 + GANANCIA) ===> GANANCIA = (PVENTA / PFINAL) - 1
		// ganancia modifica solo precio de venta
		//precio final me modifica precio sin iva y precio de venta 
		@Override
		public void afterTextChanged(Editable editable) {
			String text = editable.toString();
	        switch(view.getId()){
		        case R.id.editTextPREC_VTA://PRECIO VENTA
	        		if( PREC_VTA.isFocused()){
	        			if((text.compareToIgnoreCase("") == 0) || (text.compareToIgnoreCase(".") == 0)){
	        				text = "0";
	        			}
	        			PREC_VTA_ANTERIOR.setText(String.valueOf(DecimalRound.round(producto.getPREC_VTA(), 2)));
	        			if (P_FINAL.getText().toString().compareTo("") != 0) {
	        				GANANCIA.setText(String.valueOf(DecimalRound.round((((Double.valueOf(text) / Double.valueOf(P_FINAL.getText().toString())) - 1) * 100), 2)));							
						}
						producto.setPRECIO_MODIFICADO(true);
		        	}
					break;
		        case R.id.editTextGANANCIA://PRECIO GANANCIA
	        		if( GANANCIA.isFocused()){
	        			if((text.compareToIgnoreCase("") == 0) || (text.compareToIgnoreCase(".") == 0)){
	        				text = "0";
	        			}
	        			PREC_VTA_ANTERIOR.setText(String.valueOf(DecimalRound.round(producto.getPREC_VTA(), 2)));
		    			PREC_VTA.setText(String.valueOf(DecimalRound.round(Double.valueOf(P_FINAL.getText().toString()) * (1 + ((Double.valueOf(text))) / 100), 2)));
		    			producto.setPRECIO_MODIFICADO(true);
	        		}
					break;
				case R.id.editTextP_FINAL://PRECIO FINAL
					if( P_FINAL.isFocused()){
	        			if((text.compareToIgnoreCase("") == 0) || (text.compareToIgnoreCase(".") == 0)){
	        				text = "0";
	        			}
	        			PREC_VTA_ANTERIOR.setText(String.valueOf(DecimalRound.round(producto.getPREC_VTA(), 2)));
						if (IVA.getText().toString().compareTo("-1") != 0) {
							PS_IVA.setText(String.valueOf(DecimalRound.round((Double.valueOf(text) / (1 + (Double.valueOf(IVA.getText().toString()) / 100))), 2)));
						}
    		    		PREC_VTA.setText(String.valueOf(DecimalRound.round(Double.valueOf(text) * (1 + (Double.valueOf(GANANCIA.getText().toString()) / 100)), 2)));
    		    		producto.setPRECIO_MODIFICADO(true);
					}
					break;
				case R.id.editTextPS_IVA://PS_IVA
		        	if( PS_IVA.isFocused()){
	        			if((text.compareToIgnoreCase("") == 0) || (text.compareToIgnoreCase(".") == 0)){
	        				text = "0";
	        			}
	        			PREC_VTA_ANTERIOR.setText(String.valueOf(DecimalRound.round(producto.getPREC_VTA(), 2)));
	    				P_FINAL.setText(String.valueOf(DecimalRound.round(Double.valueOf(text) * (1 + (Double.valueOf(IVA.getText().toString()) / 100)), 2)));
	    				PREC_VTA.setText(String.valueOf(DecimalRound.round(Double.valueOf(P_FINAL.getText().toString()) * (1 + (Double.valueOf(GANANCIA.getText().toString()) / 100)), 2)));
	    				producto.setPRECIO_MODIFICADO(true);
		        	}
					break;
			}
		}
	}
	
	
	public class MyOnFocusChangeListener implements OnFocusChangeListener{

		@Override
		public void onFocusChange(View v, boolean hasFocus) {
	        String text;
			switch(v.getId()){
		        case R.id.editTextEAN://EAN
		            if (!hasFocus) {
		          	String ean = EAN.getText().toString();
		          	if (ean.compareTo(producto.getKEY_EAN()) != 0) {
		          		if (ean.compareTo("") == 0) {
		          			ean = "0";
		          			EAN.setText(ean);
		          		}
		          		producto = DBProduct.getProduct(ean);
		          		if (producto == null)
		          			producto = new Product(ean);
		          		
		          		setDataOfProduct();
						}
		            }
		            break;
	            case R.id.editTextNOM_LARGO://Nombre largo
	            	text = NOM_LARGO.getText().toString();
	            	if ((!hasFocus) && (text.compareTo("")!=0)) {
	        			setChangeofNameLarge(text);
	        			NOM_LARGO.setBackgroundResource(android.R.drawable.edit_text);
		        	}
					break; 
	            case R.id.editTextNOM_CORTO://Nombre largo
	            	text = NOM_CORTO.getText().toString();
	            	if ((!hasFocus) && (text.compareTo("")!=0)) {
	            		NOM_CORTO.setBackgroundResource(android.R.drawable.edit_text);
		        	}
					break; 
	            case R.id.editTextPREC_VTA://PRECIO VENTA
	            	if (!hasFocus) {
	        			text = PREC_VTA.getText().toString();
	        			if((text.compareToIgnoreCase("") == 0) || (text.compareToIgnoreCase(".") == 0)){
	        				PREC_VTA.setText("0");
	        			}
		        	}
					break;
		        case R.id.editTextGANANCIA://PRECIO GANANCIA
		        	if (!hasFocus) {
	        			text = GANANCIA.getText().toString();
	        			if((text.compareToIgnoreCase("") == 0) || (text.compareToIgnoreCase(".") == 0)){
	        				GANANCIA.setText("0");
	        			}
	        		}
					break;
				case R.id.editTextP_FINAL://PRECIO FINAL
					if (!hasFocus) {
	        			text = P_FINAL.getText().toString();
	        			if((text.compareToIgnoreCase("") == 0) || (text.compareToIgnoreCase(".") == 0)){
	        				P_FINAL.setText("0");
	        			}
					}
					break;
				case R.id.editTextPS_IVA://PS_IVA
					if (!hasFocus) {
	        			text = PS_IVA.getText().toString();
	        			if((text.compareToIgnoreCase("") == 0) || (text.compareToIgnoreCase(".") == 0)){
	        				PS_IVA.setText("0");
	        			}
		        	}
					break;
	        }
		            
		}
		
	}


}

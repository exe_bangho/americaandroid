package com.america.app.activities;
import com.america.R;

import android.os.Bundle;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.view.View;
import android.widget.EditText;

public class Main extends BaseActivity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		//Remover la barra de t�tulo
		//requestWindowFeature(Window.FEATURE_NO_TITLE);

		//Remover la barra de notificaciones
		// getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

		//Aplicacion sin modo ahorro de energia
		//getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		//vista vertical
		setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);  
		
		setContentView(R.layout.activity_main);
	}

	
	public void synchronizeDB(View v){
		Intent intent = new Intent(Main.this, Synchronize.class);
		startActivity(intent);
	}
	
	public void scanBar(View v) {
		Intent intent = new Intent("com.america.app.activities.SCAN");
		intent.putExtra("SCAN_MODE", "PRODUCT_MODE"/*ONE_D_MODE*/);
		startActivityForResult(intent, 0);
	}

	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		if (requestCode == 0) {
			if (resultCode == RESULT_OK) {
				String contenido = intent.getStringExtra("SCAN_RESULT");
				String formato = intent.getStringExtra("SCAN_RESULT_FORMAT");
				showProuct(contenido);
				// Hacer algo con los datos obtenidos.
			} else if (resultCode == RESULT_CANCELED) {
				// Si se cancelo la captura.
			}
		}
	}
	
	public void getProduct(View v){
		EditText EAN = (EditText) findViewById(R.id.editTextEANSearch);
		if (EAN.getText().toString().compareTo("") != 0) {
			showProuct(EAN.getText().toString());
		}
	}
	
	private void showProuct(String contenido) {
		Intent intent = new Intent(Main.this, ProductInfo.class);
		intent.putExtra("idProduct", contenido);
		startActivity(intent);
	}
}

package com.america.app.activities;

import android.os.Bundle;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.america.R;
import com.america.app.AmericaApplication;


public class BaseFragment extends SherlockFragmentActivity{
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.header_));
	}

	@Override
	protected void onStart() {
		super.onStart();
		AmericaApplication.getInstance().setCurrentActivity(this);
	}

	@Override
	protected void onResume() {
		super.onResume();
		AmericaApplication.getInstance().activityResumed();
	}

	@Override
	protected void onPause() {
		super.onPause();
		AmericaApplication.getInstance().activityPaused();
	}
}

package com.america.app.activities;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import com.america.R;
import com.america.app.AmericaApplication;
import com.america.app.ws.WSImplement;

public class Synchronize extends BaseActivity {

	private WSImplement service;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		//vista vertical
		setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);  
		
		setContentView(R.layout.activity_synchronize_data);

		AmericaApplication.getInstance().setCurrentActivity(this);
	}

	public void configuration(View v){
		Intent intent = new Intent(Synchronize.this, Configuration.class);
		startActivity(intent);
	}
	
	public void synchronizeDepartments(View v){
		service = WSImplement.getInstance();
		service.getDepartments();
	}
	
	public void synchronizeArticles(View v){
		service = WSImplement.getInstance();
		//service.getDepartments();
		service.putProducts();
	}
	
}

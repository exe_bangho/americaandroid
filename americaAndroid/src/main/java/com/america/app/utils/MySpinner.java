package com.america.app.utils;

import java.lang.reflect.Method;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.AdapterView;
import android.widget.SpinnerAdapter;
import android.widget.Spinner;

public class MySpinner extends Spinner {
	 private Context mContext;
	 private String TextSpinner;
	 
	    public MySpinner(Context context) {
	        super(context);
	        this.mContext = context;
	    }

	    public MySpinner(Context context, AttributeSet attrs) {
	        super(context, attrs);
	        this.mContext = context;
	    }

	    public MySpinner(Context context, AttributeSet attrs, int defStyle) {
	        super(context, attrs, defStyle);
	        this.mContext = context;
	    }

	    public void SetTextSpinnerDefault(String texto) {
	        this.TextSpinner = texto;
	    }  
	    
	    @Override
	    public void setAdapter(SpinnerAdapter orig ) {
	        final SpinnerAdapter adapter = newProxy(orig);

	        super.setAdapter(adapter);

	        try {
	            final Method m = AdapterView.class.getDeclaredMethod("setNextSelectedPositionInt",int.class);
	            m.setAccessible(true);
	            m.invoke(this,-1);

	            final Method n = AdapterView.class.getDeclaredMethod("setSelectedPositionInt",int.class);
	            n.setAccessible(true);
	            n.invoke(this,-1);

	        } catch( Exception e ) {
	            throw new RuntimeException(e);
	        }
	    }
	    
	    protected SpinnerAdapter newProxy(SpinnerAdapter obj) {
	        return (SpinnerAdapter) java.lang.reflect.Proxy.newProxyInstance(
	                obj.getClass().getClassLoader(),
	                new Class[]{SpinnerAdapter.class},
	                new SpinnerAdapterProxy(obj, mContext, TextSpinner));
	    } 

	    
	}
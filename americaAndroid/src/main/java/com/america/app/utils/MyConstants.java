package com.america.app.utils;

public class MyConstants {

	public static final String DATE_ACTUALIZATION = "date";
	public static final String PORT = "port";
	public static final String IP = "ip";
	public static final int timeOut = 1000000;
}

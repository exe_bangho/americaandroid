package com.america.app.utils;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import com.america.app.activities.ProductInfo;

import android.content.Context;
import android.graphics.Color;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

public class SpinnerAdapterProxy implements InvocationHandler {

    protected SpinnerAdapter obj;
    protected Method getView;
	private Context mContex;
	private String TextSpinner;

    protected SpinnerAdapterProxy(SpinnerAdapter obj, Context context, String texto) {
    	this.mContex = context;
        this.obj = obj;
        this.TextSpinner = texto;
        try {
            this.getView = SpinnerAdapter.class.getMethod("getView",int.class,View.class,ViewGroup.class);
        } catch( Exception e ) {
            throw new RuntimeException(e);
        }
    }

    protected View getView(int position, View convertView, ViewGroup parent) throws IllegalAccessException {
        if( position<0 ) {        	
           final TextView label = (TextView) ((LayoutInflater)((ProductInfo)mContex).getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(android.R.layout.simple_spinner_item,parent,false);
           label.setText(TextSpinner);
           label.setTextColor(Color.BLACK);
//           label.setTextSize(TypedValue.COMPLEX_UNIT_DIP,20); 
           return label;   		 
        }

        return obj.getView(position,convertView,parent);
    }

	@Override
    public Object invoke(Object proxy, Method m, Object[] args) throws Throwable {
        try {
            return m.equals(getView) && (Integer)(args[0])<0 ? getView((Integer)args[0],(View)args[1],(ViewGroup)args[2]) : m.invoke(obj, args);
        } catch (InvocationTargetException e) {
            throw e.getTargetException();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
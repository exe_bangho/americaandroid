package com.america.app.database;

import java.util.ArrayList;
import java.util.List;
import com.america.app.AmericaApplication;
import com.america.app.elements.Department;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class DBDepartment {

	// Productos table name
	private static final String TABLE_DEPARTMENTS = "Departamentos";
	
	// Productos Table Columns names
	private static final String CODE = "DEPTO_CODE";
	private static final String NAME = "DEPTO_NAME";
	private static final String DELETE = "DEPTO_DELETE";
	private static final String IVA = "DEPTO_IVA";
	private static final String MAXIMO = "DEPTO_MAXIMO";
	
	public static void createTableDepartments(SQLiteDatabase db) {
		
		String CREATE_DEPARTMENTS_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_DEPARTMENTS + "("
				+ CODE + " INTEGER PRIMARY KEY," 
				+ NAME + " TEXT,"
				+ DELETE + " BOOLEAN,"
				+ IVA + " DOUBLE,"
				+ MAXIMO + " DOUBLE"
				+ ")";
//		db.execSQL("DROP TABLE IF EXISTS " + TABLE_DEPARTMENTS);
//		db.execSQL("DROP TABLE IF EXISTS " + "Productos");
		
		db.execSQL(CREATE_DEPARTMENTS_TABLE);
	}
	
	public static void addDepartment(Department department) {
		SQLiteDatabase db = AmericaApplication.getInstance().getDbHandler().getReadableDatabase();
		ContentValues values = new ContentValues();

		values.put(CODE, department.getCODE());
		values.put(NAME, department.getNAME());
		values.put(DELETE, department.isDELETE());
		values.put(IVA, department.getIVA());
		values.put(MAXIMO, department.getMAXIMO());
		
		db.insert(TABLE_DEPARTMENTS, null, values);
		db.close(); // Closing database connection
	}
	
	public static int updateDepartment(Department department) {
		int updatedRows = 0;
		SQLiteDatabase db = AmericaApplication.getInstance().getDbHandler().getWritableDatabase();
		ContentValues values = new ContentValues();
		
		values.put(CODE, department.getCODE());
		values.put(NAME, department.getNAME());
		values.put(DELETE, department.isDELETE());
		values.put(IVA, department.getIVA());
		values.put(MAXIMO, department.getMAXIMO());

		// updating row, 0 if not updated rows.
		String[] whereArgs = new String[1];
		whereArgs[0] = String.valueOf(department.getCODE());
		updatedRows = db.update(TABLE_DEPARTMENTS, values, CODE + "=" +String.valueOf(department.getCODE()), null); //CODE + " = ?",	whereArgs);
		db.close();
		
		return updatedRows;
	}
	
	public static List<Department> getDepartments(){
		List<Department> departments = new ArrayList<Department>();
		
		SQLiteDatabase db = AmericaApplication.getInstance().getDbHandler().getReadableDatabase();
		
		String[] whereArgs = new String[1];
		whereArgs[0] = "0";
		Cursor cursor = db.query(TABLE_DEPARTMENTS, null, DELETE + " = ?", whereArgs, null, null, null, null);
		
		cursor.moveToFirst();
		while (!cursor.isAfterLast()){
			Department department = new Department();
			department.setCODE(cursor.getInt(0));
			department.setNAME(cursor.getString(1));
			department.setDELETE(cursor.getInt(2)>0);
			department.setIVA(cursor.getDouble(3));
			department.setMAXIMO(cursor.getDouble(4));
			
			departments.add(department);
			
			cursor.moveToNext();
		}
		cursor.close();
		
		return departments;
	}
	
}

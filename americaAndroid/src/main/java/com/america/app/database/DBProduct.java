package com.america.app.database;

import java.util.ArrayList;
import java.util.List;

import com.america.app.AmericaApplication;
import com.america.app.elements.Product;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class DBProduct {

	// Productos table name
	private static final String TABLE_PRODUCTS = "Productos";

	// Productos Table Columns names
	private static final String KEY_EAN = "EAN";
	private static final String NOM_LARGO = "NOMLARGO";
	private static final String NOM_CORTO = "NOMCORTO";
	private static final String COD_DEPTO = "CODDEPTO";
	private static final String TIPO_VTA = "TIPOVTA";
	private static final String P_FINAL = "PFINAL";
	private static final String PS_IVA = "PSIVA";
	private static final String GANANCIA = "GANANCIA";
	private static final String PREC_VTA = "PRECVTA";
	private static final String PREC_VTA_ANTERIOR = "PRECVTAANTERIOR";
	private static final String STOCK = "STOCK";
	private static final String CANT_NUEVOS = "CANTNUEVOS";
	private static final String CANT_ENVASADA = "CANTENVASADA";
	private static final String UNIDAD_ENVASADO = "UNIDADENVASADO";
	private static final String SYNC_STATUS = "sync_status";
	private static final String PRECION_MODIFICADO = "PRECIOMODIFICADO";

	public static void createTableProducts(SQLiteDatabase db) {
		String CREATE_PRODUCTS_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_PRODUCTS + "("
				+ KEY_EAN + " TEXT PRIMARY KEY," 
				+ NOM_LARGO + " TEXT,"
				+ NOM_CORTO + " TEXT,"
				+ COD_DEPTO + " INTEGER,"
				+ TIPO_VTA + " INTEGER,"
				+ P_FINAL + " DOUBLE,"
				+ PS_IVA + " DOUBLE,"
				+ GANANCIA + " DOUBLE,"
				+ PREC_VTA + " DOUBLE,"
				+ PREC_VTA_ANTERIOR + " DOUBLE,"
				+ STOCK + " INTEGER,"
				+ CANT_NUEVOS + " INTEGER,"
				+ CANT_ENVASADA + " INTEGER,"
				+ UNIDAD_ENVASADO + " INTEGER,"
				+ SYNC_STATUS + " BOOLEAN,"
				+ PRECION_MODIFICADO + " BOOLEAN"
				+ ")";
		//db.execSQL("DROP TABLE IF EXISTS " + TABLE_PRODUCTS);
		db.execSQL(CREATE_PRODUCTS_TABLE);
	}

	public static void addProduct(Product product) {
		SQLiteDatabase db = AmericaApplication.getInstance().getDbHandler().getWritableDatabase();
		ContentValues values = new ContentValues();

		values.put(KEY_EAN, product.getKEY_EAN().replaceAll("\\s+",""));
		values.put(NOM_LARGO, product.getNOM_LARGO());
		values.put(NOM_CORTO, product.getNOM_CORTO());
		values.put(COD_DEPTO, product.getCOD_DEPTO());
		values.put(TIPO_VTA, product.getTIPO_VTA());
		values.put(P_FINAL, product.getP_FINAL());
		values.put(PS_IVA, product.getPS_IVA());
		values.put(GANANCIA, product.getGANANCIA());
		values.put(PREC_VTA, product.getPREC_VTA());
		values.put(PREC_VTA_ANTERIOR, product.getPREC_VTA_ANTERIOR());
		values.put(STOCK, product.getSTOCK());
		values.put(CANT_NUEVOS, product.getCANT_NUEVOS());
		values.put(CANT_ENVASADA, product.getCANT_ENVASADA());
		values.put(UNIDAD_ENVASADO, product.getUNIDAD_ENVASADO());
		values.put(SYNC_STATUS, product.isSYNC_STATUS());
		values.put(PRECION_MODIFICADO, product.isPRECIO_MODIFICADO());

		db.insert(TABLE_PRODUCTS, null, values);
		db.close(); // Closing database connection
	}


	public static int updateProduct(Product product) {
		SQLiteDatabase db = AmericaApplication.getInstance().getDbHandler().getWritableDatabase();
		ContentValues values = new ContentValues();
		
//		values.put(KEY_EAN, product.getKEY_EAN());
		values.put(NOM_LARGO, product.getNOM_LARGO());
		values.put(NOM_CORTO, product.getNOM_CORTO());
		values.put(COD_DEPTO, product.getCOD_DEPTO());
		values.put(TIPO_VTA, product.getTIPO_VTA());
		values.put(P_FINAL, product.getP_FINAL());
		values.put(PS_IVA, product.getPS_IVA());
		values.put(GANANCIA, product.getGANANCIA());
		values.put(PREC_VTA, product.getPREC_VTA());
		values.put(PREC_VTA_ANTERIOR, product.getPREC_VTA_ANTERIOR());
		values.put(STOCK, product.getSTOCK());
		values.put(CANT_NUEVOS, product.getCANT_NUEVOS());
		values.put(CANT_ENVASADA, product.getCANT_ENVASADA());
		values.put(UNIDAD_ENVASADO, product.getUNIDAD_ENVASADO());
		values.put(SYNC_STATUS, product.isSYNC_STATUS());
		values.put(PRECION_MODIFICADO, product.isPRECIO_MODIFICADO());

		// updating row, 0 if not updated rows.
		int updatedRows = db.update(TABLE_PRODUCTS, values, KEY_EAN + " = ?",	new String[] { product.getKEY_EAN().replaceAll("\\s+","") });
//		int updatedRows = db.update(TABLE_PRODUCTS, values, KEY_EAN + " LIKE'" + product.getKEY_EAN() + " '", null);
		db.close();
		
		return updatedRows;
	}
	
	
	public void rmProduct(Product product) {

	}


	public static Product getProduct(String idProduct) {
		Product product = null;
		SQLiteDatabase db = AmericaApplication.getInstance().getDbHandler().getReadableDatabase();
//		Cursor cursor = db.query(TABLE_PRODUCTS, null, /*KEY_EAN + "=?",new String[] { idProduct }*/KEY_EAN + " LIKE " + idProduct, null, null, null, null, null);
		Cursor cursor = db.query(TABLE_PRODUCTS, null, KEY_EAN + " LIKE '" + idProduct + "'", null, null, null, null, null);
		
//		String[] whereArgs = new String[1];
//		whereArgs[0] = idProduct;
//		cursor = db.query(true, TABLE_PRODUCTS, null, KEY_EAN + " LIKE ?", whereArgs, null, null, null, null);
		cursor.moveToFirst();
		if (!cursor.isAfterLast()){
			product = new Product(idProduct);
			product.setNOM_LARGO(cursor.getString(1));
			product.setNOM_CORTO(cursor.getString(2));
			product.setCOD_DEPTO(cursor.getInt(3));
			product.setTIPO_VTA(cursor.getInt(4));
			product.setP_FINAL(cursor.getFloat(5));
			product.setPS_IVA(cursor.getFloat(6));
			product.setGANANCIA(cursor.getFloat(7));
			product.setPREC_VTA(cursor.getFloat(8));
			product.setPREC_VTA_ANTERIOR(cursor.getFloat(9));
			product.setSTOCK(cursor.getInt(10));
			product.setCANT_NUEVOS(cursor.getInt(11));
			product.setCANT_ENVASADA(cursor.getInt(12));
			product.setUNIDAD_ENVASADO(cursor.getInt(13));
			product.setSYNC_STATUS(cursor.getInt(14)>0);
			product.setPRECIO_MODIFICADO(cursor.getInt(15)>0);
			cursor.moveToNext();
		}
		cursor.close();
		
		return product;
	}
	
	public static List<Product> getProducts() {
		List<Product> products = new ArrayList<Product>();
		
		SQLiteDatabase db = AmericaApplication.getInstance().getDbHandler().getReadableDatabase();
		
//		Cursor cursor = db.query(TABLE_PRODUCTS, null, SYNC_STATUS + "=?",new String[] { "0" }, null, null, null, null);
		Cursor cursor = db.query(TABLE_PRODUCTS, null, SYNC_STATUS + "= 0", null, null, null, null, null);
		
		cursor.moveToFirst();
		while (!cursor.isAfterLast()){
			Product product = new Product(cursor.getString(0));
			product.setNOM_LARGO(cursor.getString(1));
			product.setNOM_CORTO(cursor.getString(2));
			product.setCOD_DEPTO(cursor.getInt(3));
			product.setTIPO_VTA(cursor.getInt(4));
			product.setP_FINAL(cursor.getFloat(5));
			product.setPS_IVA(cursor.getFloat(6));
			product.setGANANCIA(cursor.getFloat(7));
			product.setPREC_VTA(cursor.getFloat(8));
			product.setPREC_VTA_ANTERIOR(cursor.getFloat(9));
			product.setSTOCK(cursor.getInt(10));
			product.setCANT_NUEVOS(cursor.getInt(11));
			product.setCANT_ENVASADA(cursor.getInt(12));
			product.setUNIDAD_ENVASADO(cursor.getInt(13));
			product.setSYNC_STATUS(cursor.getInt(14)>0);	
			product.setPRECIO_MODIFICADO(cursor.getInt(15)>0);
			products.add(product);

			cursor.moveToNext();
		}
		cursor.close();
		
		return products;
	}

}

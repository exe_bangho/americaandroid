package com.america.app.ws;

public interface WSInterface {

	public abstract void getProducts();
	
	public abstract void putProducts();
	
	public abstract void getDepartments();
}


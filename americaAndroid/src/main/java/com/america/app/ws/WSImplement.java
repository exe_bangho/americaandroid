package com.america.app.ws;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.view.Gravity;
import android.widget.Toast;
import com.america.R;
import com.america.app.AmericaApplication;
import com.america.app.database.DBDepartment;
import com.america.app.database.DBProduct;
import com.america.app.elements.Department;
import com.america.app.elements.Product;
import com.america.app.utils.MyConstants;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

public class WSImplement implements WSInterface{

	private static WSImplement instance;
	private ProgressDialog pd;
	private static RequestQueue mRequestQueue;
	private int sizeOfPage = 50;
	private String dateOfUpdate;
	

	public static WSImplement getInstance() {
		if (instance == null){
			mRequestQueue = Volley.newRequestQueue(AmericaApplication.getInstance().getCurrentActivity());
			instance = new WSImplement();
		}
		return instance;
	}

	public String getUrl(){
		return AmericaApplication.getInstance().getURL();
	}

	
	@Override
	public void getProducts(){
		dateOfUpdate = null;
		int page = 0;
//		pd = ProgressDialog.show(AmericaApplication.getInstance().getCurrentActivity(), "Please wait...", "getting products...");
		pd.setMessage(AmericaApplication.getInstance().getCurrentActivity().getString(R.string.Getting_products));
		
		String date = "";
		SharedPreferences prefs = AmericaApplication.getInstance().getPrefs();
		if (prefs.contains(MyConstants.DATE_ACTUALIZATION)){
			date = prefs.getString(MyConstants.DATE_ACTUALIZATION, "");
		}
		getProducts(page, this.sizeOfPage, 0, date);
	}
	
	public void getProducts(final int page, final int sizeOfPage, int lastPage, final String sinceDate) {
		if (lastPage > 0) {
			pd.setMessage(AmericaApplication.getInstance().getCurrentActivity().getString(R.string.Load) + " " +  String.valueOf(page *sizeOfPage) + " " + AmericaApplication.getInstance().getCurrentActivity().getString(R.string.Products_of) + " " + String.valueOf((lastPage+1)*sizeOfPage));
		}
		
		String url = getUrl() + "getArticulos?page=" + String.valueOf(page) + "&size=" + String.valueOf(sizeOfPage);
		
		if (sinceDate.compareTo("") != 0) {
			url = url +"&sinceDate=" + sinceDate.replaceAll(" ", "%20");
		}
		
		JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.GET,
												url, 
												null,
												new Response.Listener<JSONObject>() {
													@Override
													public void onResponse(JSONObject response) {
														System.out.println("Response ["+response+"]");
														int lastPage = getLastPageOfProductsParseJSON(response);
														if(page < lastPage){
															getProducts(page + 1, sizeOfPage, lastPage, sinceDate);
														}
														else {
															if (dateOfUpdate != null) { // Una vez sincronizado todo, actualizo la fecha de Sincronizacion.
																SharedPreferences prefs = AmericaApplication.getInstance().getPrefs();
																SharedPreferences.Editor editor = prefs.edit();
																editor.putString(MyConstants.DATE_ACTUALIZATION, dateOfUpdate);
																editor.commit();
															}
															pd.dismiss();
														}
													}
												}, 
												new Response.ErrorListener() {
													@Override
													public void onErrorResponse(VolleyError error) {
														System.out.println("Error ["+error+"]");
														Toast toast = Toast.makeText(AmericaApplication.getInstance().getCurrentActivity(), error.toString(), Toast.LENGTH_LONG);
														toast.setGravity(Gravity.CENTER, 0, 0);
														toast.show();
														pd.dismiss();
													}
												});

//		jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(constantes.timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
		mRequestQueue.add(jsObjRequest);
	}

	private int getLastPageOfProductsParseJSON(JSONObject response) {
		int lastPage = 0;
		try { 
			JSONObject status = response.getJSONObject("status");
			String code = status.getString("code");
			String message = status.getString("message");

			if (code.equalsIgnoreCase("200"))
			{
				JSONObject data = response.getJSONObject("data");
				this.dateOfUpdate = data.getString("date");	
				
				JSONArray plus = data.getJSONArray("plus");
				
				ArrayList<Product> products = new ArrayList<Product>();
				
				for (int i = 0; i < plus.length(); i++) {
					JSONObject producto = plus.getJSONObject(i);
					
					Product product = new Product(producto.getString("ean"));
					product.setNOM_LARGO(producto.getString("nombreLargo"));
					product.setNOM_CORTO(producto.getString("nombreCorto"));
					product.setCOD_DEPTO(producto.getInt("codDepto"));
					product.setTIPO_VTA(producto.getInt("tipoVenta"));
					product.setP_FINAL(producto.getDouble("precioFinal"));
					product.setPS_IVA(producto.getDouble("precioSinIVA"));
					product.setGANANCIA(producto.getDouble("ganancia"));
					product.setPREC_VTA(producto.getDouble("precioVta"));
					product.setPREC_VTA_ANTERIOR(producto.getDouble("precioAnterior"));
					product.setSTOCK(producto.getInt("stock"));
					product.setCANT_ENVASADA(producto.getInt("cantEnvasada"));
					product.setUNIDAD_ENVASADO(producto.getInt("unidadEnvasado"));
					
					product.setCANT_NUEVOS(0);
					product.setSYNC_STATUS(true);
					
					products.add(product);
				}
				
				for(Product product : products){
					if (DBProduct.updateProduct(product) == 0) {
						DBProduct.addProduct(product);
					}
				}
				
				lastPage = Integer.valueOf((data.getString("productTotal")));
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lastPage;
	}

	@Override
	public void putProducts() {
		pd = ProgressDialog.show(AmericaApplication.getInstance().getCurrentActivity(), AmericaApplication.getInstance().getCurrentActivity().getString(R.string.Please_wait), AmericaApplication.getInstance().getCurrentActivity().getString(R.string.Sending_products));
		List<Product> products = DBProduct.getProducts();
		putProducts(products, this.sizeOfPage, products.size());
	}
	
	public void putProducts(final List<Product> products, final int sizeOfPage, final int cantProducts) {
		List<Product> auxProducts;
		
		if (products.size() < cantProducts) {
			pd.setMessage(AmericaApplication.getInstance().getCurrentActivity().getString(R.string.Load) + " " + String.valueOf( cantProducts - products.size()) + " " + AmericaApplication.getInstance().getCurrentActivity().getString(R.string.Products_of) + " " + String.valueOf(cantProducts));
		}
		
		if (products.size() > sizeOfPage) {
			auxProducts = products.subList(0, sizeOfPage);
		}else{
			auxProducts = products;
		}
		
		JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST,
											getUrl() + "updateArticulos", 
											putProductsParseJSON(auxProducts),
											new Response.Listener<JSONObject>() {
												@Override
												public void onResponse(JSONObject response) {
													System.out.println("Response ["+response+"]");
													if (products.size() > sizeOfPage) {
														putProducts(products.subList(sizeOfPage, products.size()), sizeOfPage, cantProducts);
													}else{ 
														getProducts();
//														pd.dismiss();
													}
												}
											}, 
											new Response.ErrorListener() {
												@Override
												public void onErrorResponse(VolleyError error) {
													System.out.println("Error ["+error+"]");
													Toast toast = Toast.makeText(AmericaApplication.getInstance().getCurrentActivity(), error.toString(), Toast.LENGTH_LONG);
													toast.setGravity(Gravity.CENTER, 0, 0);
													toast.show();
													pd.dismiss();
												}
											});
//		jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(constantes.timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
		mRequestQueue.add(jsObjRequest);
	}
	
	private JSONObject putProductsParseJSON(List<Product> products){
		JSONObject result = null;
		try {
			result = new JSONObject();
			JSONArray plus = new JSONArray();
			for(int i = 0; i < products.size(); i++){
				JSONObject plu = new JSONObject();
				plu.put("ean", products.get(i).getKEY_EAN());
				plu.put("nombreLargo", products.get(i).getNOM_LARGO());
				plu.put("nombreCorto", products.get(i).getNOM_CORTO());
				plu.put("codDepto", products.get(i).getCOD_DEPTO());
				plu.put("tipoVenta", products.get(i).getTIPO_VTA());
				plu.put("precioFinal", products.get(i).getP_FINAL());
				plu.put("precioSinIVA", products.get(i).getPS_IVA());
				plu.put("ganancia", products.get(i).getGANANCIA());
				plu.put("precioVta", products.get(i).getPREC_VTA());
				plu.put("precioModificado", products.get(i).isPRECIO_MODIFICADO());
				//plu.put("precioAnterior", products.get(i).getPREC_VTA_ANTERIOR());
				plu.put("cantidadNuevos", products.get(i).getCANT_NUEVOS());
				plu.put("cantEnvasada", products.get(i).getCANT_ENVASADA());
				plu.put("unidadEnvasado", products.get(i).getUNIDAD_ENVASADO());
				plus.put(i, plu);
			}
			SharedPreferences prefs = AmericaApplication.getInstance().getPrefs();
			String date = "";
			if (prefs.contains(MyConstants.DATE_ACTUALIZATION))
				 date = prefs.getString(MyConstants.DATE_ACTUALIZATION, "");
			result.put("date", date);
			result.put("plus", plus);
			result.put("productTotal", products.size());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return result;
	}

	@Override
	public void getDepartments() {
		pd = ProgressDialog.show(AmericaApplication.getInstance().getCurrentActivity(), AmericaApplication.getInstance().getCurrentActivity().getString(R.string.Please_wait), AmericaApplication.getInstance().getCurrentActivity().getString(R.string.Getting_departments));
		
		JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.GET,
												getUrl() + "getAllDeptos", 
												null,
												new Response.Listener<JSONObject>() {
													@Override
													public void onResponse(JSONObject response) {
														System.out.println("Response ["+response+"]");
														getDepartmentsParseJSON(response);
										                pd.dismiss();
													}
												}, 
												new Response.ErrorListener() {
													@Override
													public void onErrorResponse(VolleyError error) {
														System.out.println("Error ["+error+"]");
														Toast toast = Toast.makeText(AmericaApplication.getInstance().getCurrentActivity(), error.toString(), Toast.LENGTH_LONG);
														toast.setGravity(Gravity.CENTER, 0, 0);
														toast.show();
														pd.dismiss();
													}
												});
//		jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(constantes.timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
		mRequestQueue.add(jsObjRequest);
	}

	private void getDepartmentsParseJSON(JSONObject response) {

		try { 
			JSONObject status = response.getJSONObject("status");
			String code = status.getString("code");
			String message = status.getString("message");

			if (code.equalsIgnoreCase("200"))
			{
				JSONObject data = response.getJSONObject("data");
				JSONArray deptos = data.getJSONArray("deptos");
				
				ArrayList<Department> departments = new ArrayList<Department>();
				
				for (int i = 0; i < deptos.length(); i++) {
					JSONObject depto = deptos.getJSONObject(i);
					
					Department department = new Department();
					department.setCODE(depto.getInt("codDepto"));
					department.setIVA(depto.getDouble("iva"));
					department.setMAXIMO(depto.getDouble("maximo"));
					department.setNAME(depto.getString("nombre"));
					department.setDELETE(depto.getBoolean("borrado"));
					//department.setCODIVA(depto.getDouble("codIva"));
					
					departments.add(department);
				}
				
				for(Department department : departments){
					if (DBDepartment.updateDepartment(department) == 0) {
						DBDepartment.addDepartment(department);
					}
				}
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}

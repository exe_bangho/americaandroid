package com.america.app;
import com.america.app.database.DBHandler;
import com.america.app.utils.MyConstants;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;


public class AmericaApplication extends Application{
	
	private static AmericaApplication instance;
	private Activity currentActivity;
	private DBHandler dbHandler;
	private SharedPreferences prefs;
	private boolean activityVisible;
	private MyConstants constantes;

	public AmericaApplication() {
		super();
		instance = this;
	}
	
	@Override
	public void onCreate() {
		super.onCreate(); 
		this.dbHandler = new DBHandler(getApplicationContext());
		this.setPrefs(getSharedPreferences("INFO", Context.MODE_PRIVATE));
	}

	public static AmericaApplication getInstance() {
		return instance;
	}

	public static void setInstance(AmericaApplication instance) {
		AmericaApplication.instance = instance;
	}

	public Activity getCurrentActivity() {
		return currentActivity;
	}

	public void setCurrentActivity(Activity currentActivity) {
		this.currentActivity = currentActivity;
	}

	public DBHandler getDbHandler() {
		return dbHandler;
	}

	public void setDbHandler(DBHandler dbHandler) {
		this.dbHandler = dbHandler;
	}

	public SharedPreferences getPrefs() {
		return prefs;
	}

	public void setPrefs(SharedPreferences prefs) {
		this.prefs = prefs;
	}
	
	public void activityResumed() {
		activityVisible = true;
		//ToastUtils.cancelToast();
	}

	public void activityPaused() {
		activityVisible = false;
	}

	public boolean isActivityVisible() {
		return activityVisible;
	}

	public String getURL() {

		SharedPreferences prefs = AmericaApplication.getInstance().getPrefs(); 
		String PORT = prefs.getString(constantes.PORT, "");
		String IP = prefs.getString(constantes.IP, "");
		return "http://" + IP + ":" + PORT + "/America/";
	}

}

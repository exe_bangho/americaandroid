package com.america.app.adapters;

import com.america.R;
import com.america.app.activities.ProductInfo;
import com.america.app.elements.Department;

import android.content.Context;
import android.graphics.Color;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class DepartmentAdapter extends ArrayAdapter<Department>{

	private Department[] values;
	private Context context;
	
	public DepartmentAdapter(Context context, int textViewResourceId, Department[] values) {
	    super(context, textViewResourceId, values);
        this.context = context;
        this.values = values;
	}
	
	public int getPosicion(int code){
   	 for (int i = 0; i < values.length; i++) {
			if (values[i].getCODE() == code){
				return i;
			}
				
		}
        return -1;
     }	

    // And the "magic" goes here
    // This is for the "passive" state of the spinner
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // I created a dynamic TextView here, but you can reference your own  custom layout for each spinner item
        TextView label = new TextView(context);
        label.setTextColor(Color.BLACK);
//        label.setTextSize(TypedValue.COMPLEX_UNIT_DIP,20);
        // Then you can get the current item using the values array (Users array) and the current position
        // You can NOW reference each method you has created in your bean object (User class)
        
        label.setText(values[position].getNAME());
        // And finally return your dynamic (or custom) view for each spinner item
        return label;
    }

    // And here is when the "chooser" is popped up
    // Normally is the same view, but you can customize it if you want
    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
//  		LayoutInflater inflater= ((ProductInfo) context).getLayoutInflater();
//  		View row=inflater.inflate(R.layout.row_spinner, parent, false);
//  			
//  		TextView label=(TextView)row.findViewById(R.id.data_spinner);
//  		label.setText(values[position].getNAME());
    	TextView lbl = (TextView) super.getView(position, convertView, parent);
        lbl.setText(values[position].getNAME());
    	
  		return lbl;
    }
	
}
